package XMLParsers.EventBased;

import XMLParsers.TravelVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

public class StaxParser {

    private boolean hasType = false;
    private boolean hasCountry = false;
    private boolean hasNumberOfDayAndNights = false;
    private boolean hasTransport = false;
    private boolean hasHotelType = false;
    private boolean hasHotelFood = false;
    private boolean hasHotelApartmentType = false;
    private boolean hasHotelApartmentComfort = false;
    private boolean hasCost = false;

    private int voucherID;
    private String type;
    private String country;
    private int numberOfDayAndNights;
    private String transport;
    private String hotelType;
    private String hotelFood;
    private String hotelApartmentType;
    private String hotelApartmentComfort;
    private double cost;

    private final File fileXml = new File("D:\\Epam Training\\java_xml\\src\\main\\java\\XMLEntity\\travelVoucher.xml");

    public void parseFileWithStax() throws XMLStreamException, FileNotFoundException {
        Logger logger = LogManager.getLogger();

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(fileXml));

        ArrayList<TravelVoucher> listOfTravelVouchers = new ArrayList<>();

        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch(event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:

                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();

                    if (qName.equalsIgnoreCase("travel_voucher")) {
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        String voucherID = attributes.next().getValue();
                        this.voucherID = Integer.parseInt(voucherID);
                    } else if (qName.equalsIgnoreCase("type")) {
                        hasType = true;
                    } else if (qName.equalsIgnoreCase("country")) {
                        hasCountry = true;
                    } else if (qName.equalsIgnoreCase("number_daynights")) {
                        hasNumberOfDayAndNights = true;
                    } else if (qName.equalsIgnoreCase("transport")) {
                        hasTransport = true;
                    } else if (qName.equalsIgnoreCase("hotel_type")) {
                        hasHotelType = true;
                    } else if (qName.equalsIgnoreCase("hotel_food")) {
                        hasHotelFood = true;
                    } else if (qName.equalsIgnoreCase("hotel_apartment_type")) {
                        hasHotelApartmentType = true;
                    } else if (qName.equalsIgnoreCase("hotel_apartment_comfort")) {
                        hasHotelApartmentComfort = true;
                    } else if (qName.equalsIgnoreCase("cost")) {
                        hasCost = true;
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    Characters characters = event.asCharacters();
                    if(hasType) {
                        type = characters.getData();
                        hasType = false;
                    }
                    if(hasCountry) {
                        country = characters.getData();
                        hasCountry = false;
                    }
                    if(hasNumberOfDayAndNights) {
                        numberOfDayAndNights = Integer.parseInt(characters.getData());
                        hasNumberOfDayAndNights = false;
                    }
                    if(hasTransport) {
                        transport = characters.getData();
                        hasTransport = false;
                    }
                    if(hasHotelType) {
                        hotelType = characters.getData();
                        hasHotelType = false;
                    }
                    if(hasHotelFood) {
                        hotelFood = characters.getData();
                        hasHotelFood = false;
                    }
                    if(hasHotelApartmentType) {
                        hotelApartmentType = characters.getData();
                        hasHotelApartmentType = false;
                    }
                    if(hasHotelApartmentComfort) {
                        hotelApartmentComfort = characters.getData();
                        hasHotelApartmentComfort = false;
                    }
                    if(hasCost) {
                        cost = Double.parseDouble(characters.getData());
                        hasCost = false;
                    }
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    EndElement endElement = event.asEndElement();

                    if(endElement.getName().getLocalPart().equalsIgnoreCase("travel_voucher")) {
                        listOfTravelVouchers.add(new TravelVoucher.Builder()
                                                .withID(voucherID)
                                                .withType(type)
                                                .withCountry(country)
                                                .withNumberOfDayAndNights(numberOfDayAndNights)
                                                .withTransport(transport)
                                                .withHotelType(hotelType)
                                                .withHotelFood(hotelFood)
                                                .withHotelApartmentType(hotelApartmentType)
                                                .withHotelApartmentComfort(hotelApartmentComfort)
                                                .withCost(cost)
                                                .build());
                    }
                    break;
            }
        }

        for (TravelVoucher tempTravelVoucher : listOfTravelVouchers) {
//            System.out.println(tempTravelVoucher.toString());
            logger.info(tempTravelVoucher.toString());
        }
    }
}
