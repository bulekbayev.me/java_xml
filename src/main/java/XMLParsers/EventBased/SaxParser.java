package XMLParsers.EventBased;

import XMLParsers.TravelVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SaxParser {

    private ArrayList<TravelVoucher> listOfTravelVouchers = new ArrayList<>();
    private final File fileXml = new File("D:\\Epam Training\\java_xml\\src\\main\\java\\XMLEntity\\travelVoucher.xml");

    public void parseFileWithSax() throws ParserConfigurationException, SAXException, IOException {
        Logger logger = LogManager.getLogger();

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        AdvancedXMLHandler handler = new AdvancedXMLHandler();
        parser.parse(fileXml, handler);

        for (TravelVoucher tempTravelVoucher : listOfTravelVouchers) {
//            System.out.println(tempTravelVoucher.toString());
            logger.info(tempTravelVoucher.toString());
        }
    }

    private class AdvancedXMLHandler extends DefaultHandler {
        private int voucherID;
        private String type;
        private String country;
        private int numberOfDayAndNights;
        private String transport;
        private String hotelType;
        private String hotelFood;
        private String hotelApartmentType;
        private String hotelApartmentComfort;
        private double cost;
        private String lastElementName;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            lastElementName = qName;
            int elementAttributesLength = attributes.getLength();
            for (int i = 0; i < elementAttributesLength; i++) {
                if (attributes.getQName(i).equals("id")) {
                voucherID = Integer.parseInt(attributes.getValue(i));
                }
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            String information = new String(ch, start, length);

            information = information.replace("\n", "").trim();

            if (!information.isEmpty()) {
                if (lastElementName.equals("type"))
                    type = information;
                if (lastElementName.equals("country"))
                    country = information;
                if (lastElementName.equals("number_daynights"))
                    numberOfDayAndNights = Integer.parseInt(information);
                if (lastElementName.equals("transport"))
                    transport = information;
                if (lastElementName.equals("hotel_type"))
                    hotelType = information;
                if (lastElementName.equals("hotel_food"))
                    hotelFood = information;
                if (lastElementName.equals("hotel_apartment_type"))
                    hotelApartmentType = information;
                if (lastElementName.equals("hotel_apartment_comfort"))
                    hotelApartmentComfort = information;
                if (lastElementName.equals("cost"))
                    cost = Double.parseDouble(information);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if (qName.equals("travel_voucher")) {
                listOfTravelVouchers.add(new TravelVoucher.Builder()
                                                .withID(voucherID)
                                                .withType(type)
                                                .withCountry(country)
                                                .withNumberOfDayAndNights(numberOfDayAndNights)
                                                .withTransport(transport)
                                                .withHotelType(hotelType)
                                                .withHotelFood(hotelFood)
                                                .withHotelApartmentType(hotelApartmentType)
                                                .withHotelApartmentComfort(hotelApartmentComfort)
                                                .withCost(cost)
                                                .build());
            }
        }
    }
}
