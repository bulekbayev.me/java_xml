package XMLParsers.ObjectBased;

import XMLParsers.TravelVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DomParser {

    private int voucherID;
    private String type;
    private String country;
    private int numberOfDayAndNights;
    private String transport;
    private String hotelType;
    private String hotelFood;
    private String hotelApartmentType;
    private String hotelApartmentComfort;
    private double cost;

    private final File fileXml = new File("D:\\Epam Training\\java_xml\\src\\main\\java\\XMLEntity\\travelVoucher.xml");

    public void parseFileWithDom() throws ParserConfigurationException, IOException, SAXException {
        Logger logger; logger = LogManager.getLogger();

        DocumentBuilderFactory factoryDom = DocumentBuilderFactory.newInstance();
        DocumentBuilder builderDom = factoryDom.newDocumentBuilder();
        Document document = builderDom.parse(fileXml);

        NodeList travelVoucherNodeList = document.getElementsByTagName("travel_voucher");

        ArrayList<TravelVoucher> listOfTravelVouchers = new ArrayList<>();

        for (int i = 0; i < travelVoucherNodeList.getLength(); i++) {

            Element travelVoucherID = (Element) document.getElementsByTagName("travel_voucher").item(i);
            voucherID = Integer.parseInt(travelVoucherID.getAttribute("id"));

            if (travelVoucherNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element travelVoucherElement = (Element) travelVoucherNodeList.item(i);

                NodeList childNodesOfTravelVoucher = travelVoucherElement.getChildNodes();
                for (int j = 0; j < childNodesOfTravelVoucher.getLength(); j++) {
                    if (childNodesOfTravelVoucher.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        Element childNodeElement = (Element) childNodesOfTravelVoucher.item(j);

                        switch (childNodeElement.getNodeName()) {
                            case "type":
                                type = childNodeElement.getTextContent();
                                break;
                            case "country":
                                country = childNodeElement.getTextContent();
                                break;
                            case "number_daynights":
                                numberOfDayAndNights = Integer.parseInt(childNodeElement.getTextContent());
                                break;
                            case "transport":
                                transport = childNodeElement.getTextContent();
                                break;
                            case "hotel_characteristics":
                                    NodeList hotelCharacteristicsNodeList = childNodeElement.getChildNodes();
                                    hotelType = hotelCharacteristicsNodeList.item(1).getTextContent();
                                    hotelFood = hotelCharacteristicsNodeList.item(3).getTextContent();
                                    hotelApartmentType = hotelCharacteristicsNodeList.item(5).getTextContent();
                                    hotelApartmentComfort = hotelCharacteristicsNodeList.item(7).getTextContent();
                                break;
                            case "cost":
                                cost = Double.parseDouble(childNodeElement.getTextContent());
                                break;
                        }
                    }
                }
                listOfTravelVouchers.add(new TravelVoucher.Builder()
                                        .withID(voucherID)
                                        .withType(type)
                                        .withCountry(country)
                                        .withNumberOfDayAndNights(numberOfDayAndNights)
                                        .withTransport(transport)
                                        .withHotelType(hotelType)
                                        .withHotelFood(hotelFood)
                                        .withHotelApartmentType(hotelApartmentType)
                                        .withHotelApartmentComfort(hotelApartmentComfort)
                                        .withCost(cost)
                                        .build());
            }
        }

        for (TravelVoucher tempTravelVoucher : listOfTravelVouchers) {
//            System.out.println(tempTravelVoucher.toString());
            logger.info(tempTravelVoucher.toString());
        }
    }
}
