package XMLParsers;

public class TravelVoucher {

    private int travelVoucherID;
    private String type;
    private String country;
    private int numberOfDayAndNights;
    private String transport;
    private String hotelType;
    private String hotelFood;
    private String hotelApartmentType;
    private String hotelApartmentComfort;
    private double cost;

    @Override
    public String toString() {
        return "TravelVoucher " + travelVoucherID + ":" + "\n" +
                "\t" + "Type: " + type + ";\n" +
                "\t" + "Country: " + country + ";\n" +
                "\t" + "Number of day and nights: " + numberOfDayAndNights + ";\n" +
                "\t" + "Transport: " + transport + ";\n" +
                "\t" + "Hotel Characteristics: " + "\n" +
                "\t\t" + "Hotel type: " + hotelType + ";\n" +
                "\t\t" + "Hotel food: " + hotelFood + ";\n" +
                "\t\t" + "Hotel apartment type: " + hotelApartmentType + ";\n" +
                "\t\t" + "Hotel apartment comfort: " + hotelApartmentComfort + ";\n" +
                "\t" + "Price: " + cost + ";";
    }

    public static class Builder {
        private TravelVoucher newTravelVoucher;

        public Builder() {
            newTravelVoucher = new TravelVoucher();
        }

        public Builder withID(int id) {
            newTravelVoucher.travelVoucherID = id;
            return this;
        }

        public Builder withType(String type) {
            newTravelVoucher.type = type;
            return this;
        }

        public Builder withCountry(String country) {
            newTravelVoucher.country = country;
            return this;
        }

        public Builder withNumberOfDayAndNights(int numberOfDayAndNights) {
            newTravelVoucher.numberOfDayAndNights = numberOfDayAndNights;
            return this;
        }

        public Builder withTransport(String transport) {
            newTravelVoucher.transport = transport;
            return this;
        }

        public Builder withHotelType(String hotelType) {
            newTravelVoucher.hotelType = hotelType;
            return this;
        }

        public Builder withHotelFood(String hotelFood) {
            newTravelVoucher.hotelFood = hotelFood;
            return this;
        }

        public Builder withHotelApartmentType(String hotelApartmentType) {
            newTravelVoucher.hotelApartmentType = hotelApartmentType;
            return this;
        }

        public Builder withHotelApartmentComfort(String hotelApartmentComfort) {
            newTravelVoucher.hotelApartmentComfort = hotelApartmentComfort;
            return this;
        }

        public Builder withCost(double cost) {
            newTravelVoucher.cost = cost;
            return this;
        }

        public TravelVoucher build() {
            return newTravelVoucher;
        }
    }
}
