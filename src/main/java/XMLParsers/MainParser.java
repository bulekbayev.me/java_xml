package XMLParsers;

import XMLParsers.EventBased.SaxParser;
import XMLParsers.EventBased.StaxParser;
import XMLParsers.ObjectBased.DomParser;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class MainParser {

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        System.out.println("Using DOM-parser...");
        DomParser domParser = new DomParser();
        domParser.parseFileWithDom();
        System.out.println("Processed.");
        System.out.println("Using SAX-parser...");
        SaxParser saxParser = new SaxParser();
        saxParser.parseFileWithSax();
        System.out.println("Processed.");
        System.out.println("Using StAX-parser...");
        StaxParser staxParser = new StaxParser();
        staxParser.parseFileWithStax();
        System.out.println("Processed.");
        System.out.println("Check output in the file log/logfile.log");
    }
}
